TREC-IS 2020-A Task 1 Notebook Evaluator v2.4
Run: _baseline.low (marks.run)

--------------------------------------------------
EVALUATON: Information Type Categorization (Multi-type)
Per Event Performance
--------------------------------------------------
athensEarthquake2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.9479069767441862
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.9957716701902749
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9327918103927896

baltimoreFlashFlood2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.96
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.9996446339729922
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9474806419032656

brooklynBlockPartyShooting2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.8486847599164926
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.9933890048712596
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.8029886825623557

daytonOhioShooting2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.9536000000000001
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.9985964912280701
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9393905817174517

elPasoWalmartShooting2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.9441425389755012
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.9988864142538976
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9268549994139023

gilroygarlicShooting2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.92468085106383
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.999645390070922
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9010078387458008

hurricaneBarry2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.9528358208955224
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.993958777540867
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9398496240601504

indonesiaEarthquake2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.907404255319149
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.8819148936170214
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9154535274356104

keralaFloods2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.8595396419437339
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 1.0
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.8151837393996502

myanmarFloods2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.9232340425531913
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.9670212765957448
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9094064949608062

papuaNewguineaEarthquake2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.8708724832214765
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.9504101416853095
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.8457553279171082

siberianWildfires2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.921603375527426
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.9732770745428972
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9052853653120142

typhoonKrosa2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.9533047210300429
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.9957081545064378
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9399141630901287

typhoonLekima2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.943454157782516
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.9840085287846482
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.9306475143081586

whaleyBridgeCollapse2020
  Information Type Precision (positive class, multi-type, macro): 0.0
  Information Type Recall (positive class, multi-type, macro): 0.0
  Information Type F1 (positive class, multi-type, macro): 0.0
  Information Type Accuracy (overall, multi-type, macro): 0.8664761904761904
  High Importance Information Type Precision (positive class, multi-type, macro): 0.0
  High Importance Information Type Recall (positive class, multi-type, macro): 0.0
  High Importance Information Type F1 (positive class, multi-type, macro): 0.0
  High Importance Information Type Accuracy (overall, multi-type, macro): 0.8912698412698413
  Low Importance Information Type Precision (positive class, multi-type, macro): 0.0
  Low Importance Information Type Recall (positive class, multi-type, macro): 0.0
  Low Importance Information Type F1 (positive class, multi-type, macro): 0.0
  Low Importance Information Type Accuracy (overall, multi-type, macro): 0.8586466165413535


